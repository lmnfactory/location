<?php

namespace Lmn\Location\Database\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

    protected $table = 'city';

    protected $fillable = ['country_id', 'name', 'code'];
}
