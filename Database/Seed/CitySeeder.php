<?php

namespace Lmn\Location\Database\Seed;

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder {

    public function run() {
        \DB::table('city')->insert([
            [
                'id' => 1,
                'country_id' => 1,
                'name' => 'Bratislava',
                'code' => 'BA'
            ],
            [
                'id' => 2,
                'country_id' => 1,
                'name' => 'Trnava',
                'code' => 'TT'
            ],
            [
                'id' => 3,
                'country_id' => 1,
                'name' => 'Košice',
                'code' => 'KE'
            ],
            [
                'id' => 4,
                'country_id' => 1,
                'name' => 'Žilina',
                'code' => 'ZA'
            ],
            [
                'id' => 5,
                'country_id' => 1,
                'name' => 'Bánska Bystrica',
                'code' => 'BB'
            ],
            [
                'id' => 6,
                'country_id' => 1,
                'name' => 'Trenčín',
                'code' => 'TN'
            ],
            [
                'id' => 7,
                'country_id' => 1,
                'name' => 'Prešov',
                'code' => 'PO'
            ],
            [
                'id' => 8,
                'country_id' => 1,
                'name' => 'Nitra',
                'code' => 'NR'
            ]
        ]);
    }
}
