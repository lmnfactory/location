<?php

namespace Lmn\Location\Database\Seed;

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder {

    public function run() {
        \DB::table('country')->insert([
            [
                'id' => 1,
                'name' => 'Slovensko',
                'code' => 'SVK'
            ],
            [
                'id' => 2,
                'name' => 'Česká republika',
                'code' => 'CZE'
            ]
        ]);
    }
}
