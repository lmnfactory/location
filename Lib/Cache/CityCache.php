<?php

namespace Lmn\Location\Lib\Cache;

use Lmn\Core\Lib\Cache\Cacheable;

use Lmn\Location\Database\Model\City;

class CityCache implements Cacheable {

    public function cache() {
        return City::get();
    }
}
