<?php

namespace Lmn\Location\Lib\Cache;

use Lmn\Core\Lib\Cache\Cacheable;

use Lmn\Location\Database\Model\Country;

class CountryCache implements Cacheable {

    public function cache() {
        return Country::get();
    }
}
