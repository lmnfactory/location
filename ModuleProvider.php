<?php

namespace Lmn\Location;

use Lmn\Core\Provider\LmnServiceProvider;
use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Illuminate\Support\Facades\Route;

use Lmn\Core\Lib\Database\Seeder\SeederService;
use Lmn\Core\Lib\Cache\CacheService;

use Lmn\Location\Database\Seed\CountrySeeder;
use Lmn\Location\Database\Seed\CitySeeder;

use Lmn\Location\Lib\Cache\CountryCache;
use Lmn\Location\Lib\Cache\CityCache;

class ModuleProvider implements ModuleServiceProvider{

    public function register(LmnServiceProvider $provider) {

    }

    public function boot(LmnServiceProvider $provider) {
        $seederService = \App::make(SeederService::class);
        $seederService->addSeeder(CountrySeeder::class);
        $seederService->addSeeder(CitySeeder::class);

        $cacheService = \App::make(CacheService::class);
        $cacheService->add('city', function() {
            return new CityCache();
        });
        $cacheService->add('country', function() {
            return new CountryCache();
        });
    }

    public function event(LmnServiceProvider $provider) {

    }

    public function route(LmnServiceProvider $provider) {
        Route::group(['namespace' => 'Lmn\\Location\\Controller'], function() {

        });
    }
}
